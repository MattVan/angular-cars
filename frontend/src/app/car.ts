

export default class Car{
    private make:string;
    private model: string;
    private year: string;
    private mileage: number;
    
    constructor(_make:string, _model:string, _year: string, _mileage: string){
        this.make = _make;
        this.model = _model;
        this.year = _year;
        this.mileage = parseInt(_mileage);
    }
}